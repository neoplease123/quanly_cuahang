﻿CREATE DATABASE Shop
USE Shop

CREATE TABLE Product
(
	MaHangHoa varchar(255) NOT NULL PRIMARY KEY,
	TenHangHoa varchar(255),
	CheckHang varchar(255),
	NgayHetHan varchar(255)
);

SELECT * FROM Product;

INSERT INTO Product VALUES ('AAA','Nuoc Ngot' , ' Chưa' , '2021-12-20');
INSERT INTO Product VALUES ('AAB','Nuoc Khoang' , 'Có' , '2021-2-20');
INSERT INTO Product VALUES ('ABA','Luoi Cau' , ' Chưa' , '2222-2-22');
INSERT INTO Product VALUES ('BAA','Nuoc Loc' , ' Chưa' , '2021-11-12');
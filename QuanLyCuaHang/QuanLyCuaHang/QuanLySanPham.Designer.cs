﻿
namespace QuanLyCuaHang
{
    partial class QuanLySanPham
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tpQLSP = new System.Windows.Forms.TabControl();
            this.tpQuanLy = new System.Windows.Forms.TabPage();
            this.panel2 = new System.Windows.Forms.Panel();
            this.btnFind = new System.Windows.Forms.Button();
            this.txtFind = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnDELETE = new System.Windows.Forms.Button();
            this.btnUPDATE = new System.Windows.Forms.Button();
            this.btnInsert = new System.Windows.Forms.Button();
            this.listView1 = new System.Windows.Forms.ListView();
            this.dgvHH = new System.Windows.Forms.DataGridView();
            this.tpThongke = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.gbThongKe = new System.Windows.Forms.GroupBox();
            this.dtpDateTK = new System.Windows.Forms.DateTimePicker();
            this.tpQLSP.SuspendLayout();
            this.tpQuanLy.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvHH)).BeginInit();
            this.tpThongke.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.gbThongKe.SuspendLayout();
            this.SuspendLayout();
            // 
            // tpQLSP
            // 
            this.tpQLSP.Controls.Add(this.tpQuanLy);
            this.tpQLSP.Controls.Add(this.tpThongke);
            this.tpQLSP.Location = new System.Drawing.Point(0, 0);
            this.tpQLSP.Name = "tpQLSP";
            this.tpQLSP.SelectedIndex = 0;
            this.tpQLSP.Size = new System.Drawing.Size(795, 448);
            this.tpQLSP.TabIndex = 0;
            // 
            // tpQuanLy
            // 
            this.tpQuanLy.Controls.Add(this.groupBox1);
            this.tpQuanLy.Location = new System.Drawing.Point(4, 22);
            this.tpQuanLy.Name = "tpQuanLy";
            this.tpQuanLy.Padding = new System.Windows.Forms.Padding(3);
            this.tpQuanLy.Size = new System.Drawing.Size(787, 422);
            this.tpQuanLy.TabIndex = 0;
            this.tpQuanLy.Text = "Quản Lý Sản Phẩm";
            this.tpQuanLy.UseVisualStyleBackColor = true;
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.btnFind);
            this.panel2.Controls.Add(this.txtFind);
            this.panel2.Location = new System.Drawing.Point(469, 27);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(298, 54);
            this.panel2.TabIndex = 0;
            // 
            // btnFind
            // 
            this.btnFind.Location = new System.Drawing.Point(184, 8);
            this.btnFind.Name = "btnFind";
            this.btnFind.Size = new System.Drawing.Size(97, 23);
            this.btnFind.TabIndex = 1;
            this.btnFind.Text = "Tìm Hàng Hóa";
            this.btnFind.UseVisualStyleBackColor = true;
            this.btnFind.Click += new System.EventHandler(this.btnFind_Click);
            // 
            // txtFind
            // 
            this.txtFind.Location = new System.Drawing.Point(12, 8);
            this.txtFind.Name = "txtFind";
            this.txtFind.Size = new System.Drawing.Size(152, 20);
            this.txtFind.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.panel2);
            this.groupBox1.Controls.Add(this.btnDELETE);
            this.groupBox1.Controls.Add(this.btnUPDATE);
            this.groupBox1.Controls.Add(this.btnInsert);
            this.groupBox1.Controls.Add(this.listView1);
            this.groupBox1.Controls.Add(this.dgvHH);
            this.groupBox1.Location = new System.Drawing.Point(8, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(773, 410);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Quản Lý Sản Phẩm";
            // 
            // btnDELETE
            // 
            this.btnDELETE.Location = new System.Drawing.Point(296, 27);
            this.btnDELETE.Name = "btnDELETE";
            this.btnDELETE.Size = new System.Drawing.Size(132, 54);
            this.btnDELETE.TabIndex = 4;
            this.btnDELETE.Text = "DELETE";
            this.btnDELETE.UseVisualStyleBackColor = true;
            // 
            // btnUPDATE
            // 
            this.btnUPDATE.Location = new System.Drawing.Point(155, 27);
            this.btnUPDATE.Name = "btnUPDATE";
            this.btnUPDATE.Size = new System.Drawing.Size(126, 54);
            this.btnUPDATE.TabIndex = 3;
            this.btnUPDATE.Text = "UPDATE";
            this.btnUPDATE.UseVisualStyleBackColor = true;
            // 
            // btnInsert
            // 
            this.btnInsert.Location = new System.Drawing.Point(26, 27);
            this.btnInsert.Name = "btnInsert";
            this.btnInsert.Size = new System.Drawing.Size(113, 54);
            this.btnInsert.TabIndex = 2;
            this.btnInsert.Text = "INSERT";
            this.btnInsert.UseVisualStyleBackColor = true;
            this.btnInsert.Click += new System.EventHandler(this.btnInsert_Click);
            // 
            // listView1
            // 
            this.listView1.HideSelection = false;
            this.listView1.Location = new System.Drawing.Point(7, 19);
            this.listView1.Name = "listView1";
            this.listView1.Size = new System.Drawing.Size(453, 75);
            this.listView1.TabIndex = 1;
            this.listView1.UseCompatibleStateImageBehavior = false;
            // 
            // dgvHH
            // 
            this.dgvHH.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvHH.Location = new System.Drawing.Point(0, 100);
            this.dgvHH.Name = "dgvHH";
            this.dgvHH.Size = new System.Drawing.Size(767, 304);
            this.dgvHH.TabIndex = 0;
            // 
            // tpThongke
            // 
            this.tpThongke.Controls.Add(this.gbThongKe);
            this.tpThongke.Controls.Add(this.dataGridView1);
            this.tpThongke.Location = new System.Drawing.Point(4, 22);
            this.tpThongke.Name = "tpThongke";
            this.tpThongke.Padding = new System.Windows.Forms.Padding(3);
            this.tpThongke.Size = new System.Drawing.Size(793, 425);
            this.tpThongke.TabIndex = 1;
            this.tpThongke.Text = "Thống Kê";
            this.tpThongke.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(7, 73);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.Size = new System.Drawing.Size(777, 346);
            this.dataGridView1.TabIndex = 0;
            // 
            // gbThongKe
            // 
            this.gbThongKe.Controls.Add(this.dtpDateTK);
            this.gbThongKe.Location = new System.Drawing.Point(9, 6);
            this.gbThongKe.Name = "gbThongKe";
            this.gbThongKe.Size = new System.Drawing.Size(775, 45);
            this.gbThongKe.TabIndex = 1;
            this.gbThongKe.TabStop = false;
            this.gbThongKe.Text = "Thống Kê trong 1 ngày";
            // 
            // dtpDateTK
            // 
            this.dtpDateTK.Location = new System.Drawing.Point(263, 19);
            this.dtpDateTK.Name = "dtpDateTK";
            this.dtpDateTK.Size = new System.Drawing.Size(223, 20);
            this.dtpDateTK.TabIndex = 0;
            // 
            // QuanLySanPham
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(836, 474);
            this.Controls.Add(this.tpQLSP);
            this.Name = "QuanLySanPham";
            this.Text = "Quản Lý Sản Phẩm";
            this.Load += new System.EventHandler(this.QuanLySanPham_Load);
            this.tpQLSP.ResumeLayout(false);
            this.tpQuanLy.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvHH)).EndInit();
            this.tpThongke.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.gbThongKe.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tpQLSP;
        private System.Windows.Forms.TabPage tpQuanLy;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button btnDELETE;
        private System.Windows.Forms.Button btnUPDATE;
        private System.Windows.Forms.Button btnInsert;
        private System.Windows.Forms.ListView listView1;
        private System.Windows.Forms.DataGridView dgvHH;
        private System.Windows.Forms.TabPage tpThongke;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnFind;
        private System.Windows.Forms.TextBox txtFind;
        private System.Windows.Forms.GroupBox gbThongKe;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.DateTimePicker dtpDateTK;
    }
}
﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
namespace QuanLyCuaHang
{
    class Functions
    {
        public static SqlConnection con;
        public static void Connect()
        {
            con = new SqlConnection();
            con.ConnectionString = Properties.Settings.Default.QLCH;
            con.Open();
            if (con.State == ConnectionState.Open)
            {
                MessageBox.Show("Ket noi thanh cong");
            }
            else
                MessageBox.Show("Ket noi khong thanh cong");
        }
        public static void Disconnect()
        {
            if (con.State == ConnectionState.Closed)
            {
                con.Close();
                con.Dispose();
                con = null;
            }
        }
        public static DataTable GetDataTable(string sql)
        {
            DataTable table = new DataTable();
            SqlDataAdapter dap = new SqlDataAdapter(sql, con);
            dap.Fill(table);
            return table;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace QuanLyCuaHang
{
    public partial class QuanLySanPham : Form
    {
        public QuanLySanPham()
        {
            InitializeComponent();
        }

        private void QuanLySanPham_Load(object sender, EventArgs e)
        {
            LoadDataGridView();
        }
        private void LoadDataGridView()
        {
            string sql;
            sql = "SELECT * FROM SanPham";
            dgvHH.DataSource = Functions.GetDataTable(sql);
            dgvHH.Columns[1].HeaderText = " Tên Hàng Hóa";
            dgvHH.Columns[1].Width = 100;
            dgvHH.Columns[2].HeaderText = " Mã Hàng Hóa";
            dgvHH.Columns[2].Width = 100;
            dgvHH.Columns[3].HeaderText = " Giá";
            dgvHH.Columns[3].Width = 100;
            dgvHH.Columns[4].HeaderText = " Kiểm Tra Hàng Hóa";
            dgvHH.Columns[4].Width = 100;
            dgvHH.Columns[5].HeaderText = " Ngày Hết Hạn ";
            dgvHH.Columns[5].Width = 100;
            dgvHH.AllowUserToAddRows = false;
            dgvHH.EditMode = DataGridViewEditMode.EditProgrammatically;
        }

        private void btnFind_Click(object sender, EventArgs e)
        {
            string name = txtFind.Text.Trim();
            string sql = string.Format("SELECT * FROM SanPham WHERE idSanpham = N'{0}'", name);
            DataSet ds = new DataSet();
            SqlDataAdapter da = new SqlDataAdapter(sql, Functions.con);
            da.Fill(ds, "SanPham");
            dgvHH.DataSource = Functions.GetDataTable(sql);
            if (ds.Tables["SanPham"].Rows.Count > 0)
            {
                dgvHH.DataSource = ds.Tables["SanPham"];
            }
            else
                MessageBox.Show("Khong tim thay");
            Functions.con.Close();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            ChucNangForm.INSERT insert = new ChucNangForm.INSERT();
            insert.ShowDialog();
        }
    }
}
